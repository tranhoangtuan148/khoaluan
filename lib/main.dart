import 'dart:async';
import 'dart:html';
import 'dart:convert';
import 'dart:ui';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:khoaluan_totnghiep/views/page1.dart';
import 'package:khoaluan_totnghiep/views/page2.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        body: PageOne(),
      ),
    );
  }
}
